var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var wallet = new Schema({
    username: String,
    privatekey: String,
    address: String,    
});

module.exports = mongoose.model('wallet', wallet);